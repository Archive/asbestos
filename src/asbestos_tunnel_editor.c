#include "config.h"
#include "asbestos_tunnel_editor.h"
#include "asbestos_gconf.h"
#include <libgnome/gnome-macros.h>
#include <glade/glade-xml.h>
#include <glib/gi18n.h>
#include <gtk/gtkmarshal.h>

GNOME_CLASS_BOILERPLATE(AsbestosTunnelEditor,
                        asbestos_tunnel_editor,
			GtkWindow,
			GTK_TYPE_WINDOW);

static void asbestos_tunnel_editor_class_init (AsbestosTunnelEditorClass *klass);
static void asbestos_tunnel_editor_finalize (GObject *obj);

enum
{
   DESTINATION_COL = 0,
   PORT_COL,
   NUMBER_OF_COLS
};

static void 
asbestos_tunnel_editor_class_init (AsbestosTunnelEditorClass *klass)
{
   GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = asbestos_tunnel_editor_finalize;
}

static void 
asbestos_tunnel_editor_finalize (GObject *obj)
{
   AsbestosTunnelEditor *self = ASBESTOS_TUNNEL_EDITOR(obj);

   if(self->id != NULL) g_free(self->id);
   gconf_client_notify_remove(self->gconf_client, self->gconf_notify);

   if(G_OBJECT_CLASS (parent_class)->finalize)
   {
      (* G_OBJECT_CLASS (parent_class)->finalize)(obj);
   }

}

static void
asbestos_tunnel_editor_instance_init (AsbestosTunnelEditor *self)
{
}

static void
selection_changed (GtkTreeSelection *selection, AsbestosTunnelEditor *self)
{
   gboolean flag;

   flag = (gtk_tree_selection_count_selected_rows(selection) > 0);
   
   gtk_widget_set_sensitive(self->remove_button, flag);
}

static void
destination_edited (GtkCellRendererText *renderer, gchar *path, gchar *new_text,
                    AsbestosTunnelEditor *self)
{
   GtkTreeIter iter;
   GtkTreeModel *model = GTK_TREE_MODEL(self->list_store);

   gtk_tree_model_get_iter_from_string(model, &iter, path);
   
   gtk_list_store_set(self->list_store, &iter,
                      DESTINATION_COL, new_text,
                      -1);
}

static void
port_edited (GtkCellRendererText *renderer, gchar *path, gchar *new_text,
             AsbestosTunnelEditor *self)
{
   GtkTreeIter iter;
   GtkTreeModel *model = GTK_TREE_MODEL(self->list_store);
   gint port;

   port = (guint16)g_ascii_strtod(new_text, NULL);

   gtk_tree_model_get_iter_from_string(model, &iter, path);
   
   gtk_list_store_set(self->list_store, &iter, PORT_COL, port, -1);
}

static void
add_button_clicked (GtkButton *button, AsbestosTunnelEditor *self)
{
   asbestos_tunnel_editor_add_row(self, "New Destination", 1234);
   /* TODO: automatically start editing this new row */
}

static void
remove_button_clicked (GtkButton *button, AsbestosTunnelEditor *self)
{
   GtkTreeIter iter;

   gtk_tree_selection_get_selected(self->selection, NULL, &iter);
   gtk_list_store_remove(self->list_store, &iter);
}

static void
cancel_button_clicked (GtkButton *button, AsbestosTunnelEditor *self)
{
   gtk_widget_hide(GTK_WIDGET(self));
}

static void
save_button_clicked (GtkButton *button, AsbestosTunnelEditor *self)
{
   GtkTreeIter iter;
   int n_rows;

#define CRAM(xxx) asbestos_gconf_tunnel_set_ ## xxx (self->gconf_client, \
   self->id, gtk_entry_get_text(GTK_ENTRY(self->xxx ## _entry)))

   CRAM(name);
   CRAM(destination);
   CRAM(ssh_options);

#undef CRAM

   n_rows = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(self->list_store),
                                           NULL);
   if(n_rows > 0)
   {
      GSList *entries = NULL;
      int i;

      for(i = 0; i < n_rows; i++)
      {
         gchar *str, *destination;
         gint port;

         gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->list_store),
                                       &iter, NULL, i);

         gtk_tree_model_get(GTK_TREE_MODEL(self->list_store), &iter,
                            DESTINATION_COL, &destination,
                            PORT_COL, &port,
                            -1);

         str = g_strdup_printf("%s:%d", destination, (guint16)port);
         entries = g_slist_prepend(entries, str);
         g_free(destination);
      } 
      entries = g_slist_reverse(entries);
      asbestos_gconf_tunnel_set_entries(self->gconf_client, self->id, entries);
      g_slist_foreach(entries, (GFunc)g_free, NULL);
      g_slist_free(entries);
   }

   gtk_widget_hide(GTK_WIDGET(self));
}

static void
gconf_changed (GConfClient *client, guint cnxn_id, 
               GConfEntry *entry, AsbestosTunnelEditor *self)
{
   gchar *str;
   GSList *entries, *node;
   GtkTreeIter iter;

#define SNARF(xxx) \
   { \
      str = asbestos_gconf_tunnel_get_ ## xxx (client, self->id); \
      if(str == NULL) str = g_strdup(""); \
      gtk_entry_set_text(GTK_ENTRY(self->xxx ## _entry), str); \
      g_free(str); \
   }

   SNARF(name);
   SNARF(destination);
   SNARF(ssh_options);

#undef SNARF

   entries = asbestos_gconf_tunnel_get_entries(client, self->id);

   /* FIXME: oh god, I'm going to dump my whole list & recreate it, just 
    *        because I'm too lazy to do this the right way at the moment.
    */

   if(gtk_tree_model_get_iter_first(GTK_TREE_MODEL(self->list_store), &iter))
   {
      while(gtk_list_store_remove(self->list_store, &iter)) ; /* Do nothing */
   }
   for(node = entries; node != NULL; node = node->next)
   {
      guint16 port = 0;
      gchar *ptr = (gchar *)(node->data);
      while(*ptr != ':' && *ptr != '\0') ptr++;
      if(*ptr == ':')
      {
         *ptr = '\0';
         ptr++;
         port = (guint16)g_ascii_strtod(ptr, NULL);
      }
      asbestos_tunnel_editor_add_row(self, (gchar *)node->data, port);
   }

   g_slist_foreach(entries, (GFunc)g_free, NULL);
   g_slist_free(entries);
}

static void
window_hidden (AsbestosTunnelEditor *self)
{
   /* Just force a reload of everything, since we don't care what we had
    * on screen
    */
   gconf_changed(self->gconf_client, self->gconf_notify, NULL, self);
}

/**
 * asbestos_tunnel_editor_new:
 * 
 * Returns: a newly allocated #AsbestosTunnelEditor
 */
AsbestosTunnelEditor *
asbestos_tunnel_editor_new (GConfClient *gconf_client, const gchar *id)
{
   GladeXML *xml;
   GtkWidget *widget;
   GtkWindow *window;
   GtkCellRenderer *renderer;
   gchar *path;
   GError *error = NULL;
   AsbestosTunnelEditor *self;

   
   self = g_object_new(ASBESTOS_TUNNEL_EDITOR_TYPE, NULL);
   gtk_window_set_type_hint(GTK_WINDOW(self), GDK_WINDOW_TYPE_HINT_DIALOG);

   self->id = g_strdup(id);
   self->gconf_client = gconf_client;

   /* Now let's build the gui */
   xml = glade_xml_new(GLADEDIR "asbestos.glade", "asbestos_tunnel_editor", 
                       NULL);
   window = GTK_WINDOW(glade_xml_get_widget(xml, "asbestos_tunnel_editor"));

   /* Clone properties we care about from the glade-created window */
   gtk_window_set_title(GTK_WINDOW(self), gtk_window_get_title(window));

   /* Now move the window contents from the glade-created window */
   widget = gtk_bin_get_child(GTK_BIN(window));
   g_object_ref(widget);
   gtk_container_remove(GTK_CONTAINER(window), widget);
   gtk_container_add(GTK_CONTAINER(self), widget);
   g_object_unref(widget);

   /* And finally, throw away the glade-created window */
   gtk_widget_destroy(GTK_WIDGET(window));
   window = NULL;

   /* Nab some widgets for future use */
   self->add_button        = glade_xml_get_widget(xml, "add_button");
   self->remove_button     = glade_xml_get_widget(xml, "remove_button");
   self->name_entry        = glade_xml_get_widget(xml, "name_entry");
   self->destination_entry = glade_xml_get_widget(xml, "destination_entry");
   self->ssh_options_entry = glade_xml_get_widget(xml, "ssh_options_entry");
   self->save_button       = glade_xml_get_widget(xml, "save_button");
   self->cancel_button     = glade_xml_get_widget(xml, "cancel_button");

   /* Hookup the treeview */
   self->tree_view = GTK_TREE_VIEW(glade_xml_get_widget(xml, 
                                                        "entries_treeview"));
   self->list_store = gtk_list_store_new(NUMBER_OF_COLS, G_TYPE_STRING, 
                                         G_TYPE_UINT);
   gtk_tree_view_set_model(self->tree_view,
                           GTK_TREE_MODEL(self->list_store));

   renderer = gtk_cell_renderer_text_new();
   g_object_set(renderer, "editable-set", TRUE, "editable", TRUE, NULL);
   g_signal_connect(renderer, "edited", G_CALLBACK(destination_edited), self);
   gtk_tree_view_insert_column_with_attributes(self->tree_view,
                                               -1,
                                               "Destination",
                                               renderer,
					       "text", DESTINATION_COL, 
					       NULL);

   renderer = gtk_cell_renderer_text_new();
   g_object_set(renderer, "editable-set", TRUE, "editable", TRUE, NULL);
   g_signal_connect(renderer, "edited", G_CALLBACK(port_edited), self);
   gtk_tree_view_insert_column_with_attributes(self->tree_view,
                                               -1,
                                               "Port",
                                               renderer,
					       "text", PORT_COL, 
					       NULL);
   self->selection = gtk_tree_view_get_selection(self->tree_view);
   g_signal_connect(self->selection, "changed", 
                    G_CALLBACK(selection_changed), self);

   /* Hookup buttons */

   g_signal_connect(self->add_button, "clicked", 
                    G_CALLBACK(add_button_clicked), self);
   g_signal_connect(self->remove_button, "clicked", 
                    G_CALLBACK(remove_button_clicked), self);
   g_signal_connect(self->save_button, "clicked", 
                    G_CALLBACK(save_button_clicked), self);
   g_signal_connect(self->cancel_button, "clicked", 
                    G_CALLBACK(cancel_button_clicked), self);

   /* Suck in initial values */
   path = asbestos_gconf_tunnel_get_gconf_path(self->gconf_client, self->id);
   /* FIXME: Should I really be listening for changes? It'd be nasty if your 
    *        unsaved changes were stomped because of action from elsewhere... 
    *        Of course in general that shouldn't ever happen, so I don't 
    *        know....
    */
   self->gconf_notify = gconf_client_notify_add(self->gconf_client, path,
                                           (GConfClientNotifyFunc)gconf_changed,
                                                self, NULL, &error);
   /* FIXME: handle potential errors */
   g_free(path);
   gconf_changed(self->gconf_client, self->gconf_notify, NULL, self);
   

   g_signal_connect(self, "delete_event", 
                    G_CALLBACK(gtk_widget_hide_on_delete), NULL);
   g_signal_connect(self, "hide", G_CALLBACK(window_hidden), NULL);

   /* Done */
   g_object_unref(xml);
   return self;
}

void
asbestos_tunnel_editor_add_row (AsbestosTunnelEditor *self, const gchar *dest,
                                guint16 port)
{
   GtkTreeIter iter;

   gtk_list_store_append(self->list_store, &iter);
   gtk_list_store_set(self->list_store, &iter,
                      DESTINATION_COL, dest,
                      PORT_COL, port,
                      -1);
}

