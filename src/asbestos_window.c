#include "config.h"
#include "asbestos_window.h"
#include "asbestos_gconf.h"
#include "asbestos_tunnel_editor.h"
#include <libgnome/gnome-macros.h>
#include <glade/glade-xml.h>
#include <glib/gi18n.h>
#include <gtk/gtkmarshal.h>

GNOME_CLASS_BOILERPLATE(AsbestosWindow,
                        asbestos_window,
			GtkWindow,
			GTK_TYPE_WINDOW);

static void asbestos_window_class_init (AsbestosWindowClass *klass);
static void asbestos_window_finalize (GObject *obj);

enum
{
   CONNECTED_COL = 0,
   ID_COL,
   NAME_COL,
   TUNNEL_COL,
   EDITOR_COL,
   FLAG_COL, /* Used when we get list updates */
   NUMBER_OF_COLS
};

static void 
asbestos_window_class_init (AsbestosWindowClass *klass)
{
   GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = asbestos_window_finalize;
}

static void 
asbestos_window_finalize (GObject *obj)
{
   AsbestosWindow *self = ASBESTOS_WINDOW(obj);

   gconf_client_notify_remove(self->gconf_client, self->gconf_notify);

   if(G_OBJECT_CLASS (parent_class)->finalize)
   {
      (* G_OBJECT_CLASS (parent_class)->finalize)(obj);
   }
}

static void
asbestos_window_instance_init (AsbestosWindow *self)
{
}

static void
connected_toggled (GtkCellRendererToggle *cell_renderer,
                   gchar *path,
                   AsbestosWindow *self)
{
   GtkTreeIter iter;
   gboolean connected;
   AsbestosTunnel *tunnel;
   gchar *id;
   
   gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(self->list_store),
                                       &iter, path);
   
   gtk_tree_model_get(GTK_TREE_MODEL(self->list_store), &iter, 
                      CONNECTED_COL, &connected,
                      TUNNEL_COL, &tunnel,
                      ID_COL, &id,
                      -1);
   
   if(connected)
   {
#ifdef DEBUG
      printf("Disconnect called from the GUI\n");
#endif
      asbestos_tunnel_disconnect(tunnel);
   } else {
#ifdef DEBUG
      printf("Connect called from the GUI\n");
#endif
      if(tunnel == NULL)
      {
         tunnel = asbestos_tunnel_new(self->gconf_client, id);
         gtk_list_store_set(self->list_store, &iter, TUNNEL_COL, tunnel, -1);
         g_object_unref(tunnel);
      }
      asbestos_tunnel_connect(tunnel);
   }

   g_free(id);

   gtk_list_store_set(self->list_store, &iter, CONNECTED_COL, !connected, -1);
}

static void
name_edited (GtkCellRendererText *cellrenderertext, gchar *path, 
             gchar *new_name, AsbestosWindow *self)
{
   gchar *old_name = NULL;
   gchar *id = NULL;
   GtkTreeIter iter;

   gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(self->list_store),
                                       &iter, path);

   gtk_tree_model_get(GTK_TREE_MODEL(self->list_store), &iter, 
                      ID_COL, &id, 
                      NAME_COL, &old_name, 
                      -1);
   if(g_str_equal(old_name, new_name)) return;

   asbestos_gconf_tunnel_set_name(self->gconf_client, id, new_name);
   gtk_list_store_set(self->list_store, &iter, NAME_COL, new_name, -1);
}

static void
selection_changed (GtkTreeSelection *selection, AsbestosWindow *self)
{
   gboolean flag;

   flag = (gtk_tree_selection_count_selected_rows(selection) > 0);
   
   gtk_widget_set_sensitive(self->properties_button, flag);
   gtk_widget_set_sensitive(self->delete_button, flag);
}

static void
new_button_clicked (GtkButton *button, AsbestosWindow *self)
{
   asbestos_gconf_create_tunnel(self->gconf_client);
}

static void
delete_button_clicked (GtkButton *button, AsbestosWindow *self)
{
   GtkTreeIter iter;
   gchar *id;
   GtkWidget *editor;

   if(!gtk_tree_selection_get_selected(self->selection, NULL, &iter))
   {
      /* Nothing's selected? Bug if we're here, since the button should be
       * insensitive if nothing's selected
       */
      gtk_widget_set_sensitive(self->properties_button, FALSE);
      return;
   }

   gtk_tree_model_get(GTK_TREE_MODEL(self->list_store), &iter, 
                      ID_COL, &id, 
                      EDITOR_COL, &editor, 
                      -1);

   if(editor) gtk_widget_hide(editor);
   asbestos_gconf_remove_tunnel_from_list(self->gconf_client, id);
}

static void
properties_button_clicked (GtkButton *button, AsbestosWindow *self)
{
   GtkTreeIter iter;
   gchar *id;
   GtkWidget *editor;

   if(!gtk_tree_selection_get_selected(self->selection, NULL, &iter))
   {
      /* Nothing's selected? Bug if we're here, since the button should be
       * insensitive if nothing's selected
       */
      gtk_widget_set_sensitive(self->properties_button, FALSE);
      return;
   }

   gtk_tree_model_get(GTK_TREE_MODEL(self->list_store), &iter, 
                      ID_COL, &id, 
                      EDITOR_COL, &editor, 
                      -1);

   if(editor == NULL)
   {
      editor = GTK_WIDGET(asbestos_tunnel_editor_new(self->gconf_client, id));
      gtk_list_store_set(self->list_store, &iter, EDITOR_COL, editor, -1);
      g_object_unref(editor);
   }
   gtk_widget_show_all(editor);

   g_free(id);
}

static void
asbestos_window_update_tunnel (const gchar *id, AsbestosWindow *self)
{
   GtkTreeIter iter;
   gboolean found = FALSE;
   gchar *name;
   GtkTreeModel *model = GTK_TREE_MODEL(self->list_store);

   if(gtk_tree_model_iter_nth_child(model, &iter, NULL, 0))
   {
      gchar *tree_id = NULL;

      do
      {
         if(tree_id != NULL) 
         {
            g_free(tree_id);
            tree_id = NULL;
         }
         gtk_tree_model_get(model, &iter, ID_COL, &tree_id, -1);
         if(g_str_equal(id, tree_id))
         {
            found = TRUE;
            break;
         }
      } while(gtk_tree_model_iter_next(model, &iter));

      if(tree_id != NULL) 
      {
         g_free(tree_id);
         tree_id = NULL;
      }
   }

   if(!found)
   {
      gtk_list_store_append(self->list_store, &iter);
      gtk_list_store_set(self->list_store, &iter, ID_COL, id, -1);
   }

   name = asbestos_gconf_tunnel_get_name(self->gconf_client, id);
   gtk_list_store_set(self->list_store, &iter, 
                      NAME_COL, name, 
                      FLAG_COL, TRUE, 
                      -1);
   g_free(name);
}

static void
gconf_changed (GConfClient *client, guint cnxn_id,
               GConfEntry *entry, AsbestosWindow *self)
{
   GSList *ids;
   GtkTreeIter iter;
   GtkTreeModel *model = GTK_TREE_MODEL(self->list_store);


   /* Clear the flag on everything first */
   if(gtk_tree_model_iter_nth_child(model, &iter, NULL, 0))
   {
      do
      {
         gtk_list_store_set(self->list_store, &iter, FLAG_COL, FALSE, -1);
      } while(gtk_tree_model_iter_next(model, &iter));
   }


   /* Update anything that's in our list */
   ids = asbestos_gconf_get_tunnels(self->gconf_client);
   g_slist_foreach(ids, (GFunc)asbestos_window_update_tunnel, self);
   g_slist_foreach(ids, (GFunc)g_free, NULL);
   g_slist_free(ids);

   /* Delete anything that wasn't flagged, since it means it's gone away */
   if(gtk_tree_model_iter_nth_child(model, &iter, NULL, 0))
   {
      gboolean flag;
      while(1)
      {
         /* FIXME: api docs warn that is_valid is really slow... */
         if(!gtk_list_store_iter_is_valid(self->list_store, &iter)) break;
         gtk_tree_model_get(model, &iter, FLAG_COL, &flag, -1);
         if(flag)
         {
            gtk_tree_model_iter_next(model, &iter);
         } else {
            if(!gtk_list_store_remove(self->list_store, &iter)) break;
         }
      }
   }
}   


/**
 * asbestos_window_new:
 * 
 * Returns: a newly allocated #AsbestosWindow
 */
AsbestosWindow *
asbestos_window_new (void)
{
   GladeXML *xml;
   GtkWidget *widget;
   GtkWindow *window;
   GtkCellRenderer *renderer;
   AsbestosWindow *self;
   GList *pixmaps = NULL;
   GError *error = NULL;
   
   self = g_object_new(ASBESTOS_WINDOW_TYPE, NULL);


   /* Now let's build the gui */
   xml = glade_xml_new(GLADEDIR "asbestos.glade", "asbestos_main_window", NULL);
   window = GTK_WINDOW(glade_xml_get_widget(xml, "asbestos_main_window"));

   /* Clone properties we care about from the glade-created window */
   gtk_window_set_title(GTK_WINDOW(self), gtk_window_get_title(window));

   /* Now move the window contents from the glade-created window */
   widget = gtk_bin_get_child(GTK_BIN(window));
   g_object_ref(widget);
   gtk_container_remove(GTK_CONTAINER(window), widget);
   gtk_container_add(GTK_CONTAINER(self), widget);
   g_object_unref(widget);

   /* And finally, throw away the glade-created window */
   gtk_widget_destroy(GTK_WIDGET(window));
   window = NULL;

   /* Set the little picture in it */
   widget = glade_xml_get_widget(xml, "main_image");
   gtk_image_set_from_file(GTK_IMAGE(widget), IMAGEDIR "glove-48.png");

   /* Setup window icons */
   pixmaps = g_list_prepend(pixmaps,
                            gdk_pixbuf_new_from_file(IMAGEDIR "glove-16.png", 
			                             NULL));
   pixmaps = g_list_prepend(pixmaps,
                            gdk_pixbuf_new_from_file(IMAGEDIR "glove-24.png", 
			                             NULL));
   pixmaps = g_list_prepend(pixmaps,
                            gdk_pixbuf_new_from_file(IMAGEDIR "glove-32.png", 
			                             NULL));
   pixmaps = g_list_prepend(pixmaps,
                            gdk_pixbuf_new_from_file(IMAGEDIR "glove-48.png", 
			                             NULL));
   pixmaps = g_list_prepend(pixmaps,
                            gdk_pixbuf_new_from_file(IMAGEDIR "glove-110.png", 
			                             NULL));
   gtk_window_set_default_icon_list(pixmaps);
   gtk_window_set_icon_list(GTK_WINDOW(self), pixmaps);
   g_list_foreach(pixmaps, (GFunc) g_object_unref, NULL);
   g_list_free(pixmaps);

   /* Nab some widgets for future use */
   self->new_button = glade_xml_get_widget(xml, "new_button");
   self->properties_button = glade_xml_get_widget(xml, "properties_button");
   self->delete_button = glade_xml_get_widget(xml, "delete_button");
   self->tree_view = GTK_TREE_VIEW(glade_xml_get_widget(xml, 
                                                        "tunnels_treeview"));

   /* Hookup the treeview */
   self->list_store = gtk_list_store_new(NUMBER_OF_COLS, 
                                         G_TYPE_BOOLEAN, // connected
                                         G_TYPE_STRING,  // name in gconf
                                         G_TYPE_STRING,  // name
                                         G_TYPE_OBJECT,  // tunnel
                                         G_TYPE_OBJECT,  // editor window
                                         G_TYPE_BOOLEAN  // arbitrary flag
					);
   gtk_tree_view_set_model(self->tree_view,
                           GTK_TREE_MODEL(self->list_store));

   renderer = gtk_cell_renderer_toggle_new();
   g_object_set(renderer, "activatable", TRUE, NULL);
   g_signal_connect(renderer, "toggled", G_CALLBACK(connected_toggled),
                    self);
   gtk_tree_view_insert_column_with_attributes(self->tree_view,
                                               -1,
                                               "Connected",
                                               renderer,
                                               "active", CONNECTED_COL, 
					       NULL);

   renderer = gtk_cell_renderer_text_new();
   g_object_set(renderer, "editable-set", TRUE, "editable", TRUE, NULL);
   g_signal_connect(renderer, "edited", G_CALLBACK(name_edited),
                    self);
   gtk_tree_view_insert_column_with_attributes(self->tree_view,
                                               -1,
                                               "Name",
                                               renderer,
					       "text", NAME_COL, 
					       NULL);

   self->selection = gtk_tree_view_get_selection(self->tree_view);
   g_signal_connect(self->selection, "changed", 
                    G_CALLBACK(selection_changed), self);
   
   /* Call up gconf */
   /* FIXME: this should probably go somewhere more sensible */
   self->gconf_client = gconf_client_get_default();
   gconf_client_add_dir(self->gconf_client, ASBESTOS_GCONF_PATH,
                        GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

   /* FIXME: I shouldn't know this path */
   self->gconf_notify = gconf_client_notify_add(self->gconf_client, 
                                                "/apps/asbestos/tunnels",
                                           (GConfClientNotifyFunc)gconf_changed,
                                                self, NULL, 
                                                &error);
   /* FIXME: handle potential errors */
   gconf_changed(self->gconf_client, self->gconf_notify, NULL, self);

   /* Hookup the buttons */
   g_signal_connect(self->new_button, "clicked", 
                    G_CALLBACK(new_button_clicked), self);
   g_signal_connect(self->delete_button, "clicked", 
                    G_CALLBACK(delete_button_clicked), self);
   g_signal_connect(self->properties_button, "clicked", 
                    G_CALLBACK(properties_button_clicked), self);

   /* Done */
   g_object_unref(xml);
   return self;
}
