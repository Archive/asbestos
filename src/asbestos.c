#include "config.h"
#include <stdio.h>
#include <gnome.h>
#include "asbestos_window.h"

int
main (int argc, char **argv)
{
   AsbestosWindow *window;
   gnome_init(PACKAGE, PACKAGE_VERSION, argc, argv);

   window = asbestos_window_new();

   gtk_widget_show_all(GTK_WIDGET(window));


   gtk_main();
   
   return 0;
}
