#include "config.h"
#include "asbestos_tunnel.h"
#include "asbestos_router.h"
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <libgnome/gnome-macros.h>
#include <gtk/gtkmarshal.h>
#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "asbestos_router_dbus_bindings.h"

GNOME_CLASS_BOILERPLATE(AsbestosTunnel,
                        asbestos_tunnel,
			GObject,
			G_TYPE_OBJECT);

static void asbestos_tunnel_class_init (AsbestosTunnelClass *klass);
static void asbestos_tunnel_finalize (GObject *obj);

static void 
asbestos_tunnel_class_init (AsbestosTunnelClass *klass)
{
   GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

   gobject_class->finalize = asbestos_tunnel_finalize;
}

static void 
asbestos_tunnel_finalize (GObject *obj)
{
   AsbestosTunnel *self = ASBESTOS_TUNNEL(obj);

   g_free(self->ssh_options);
   g_free(self->destination);
   g_free(self->name);
   g_free(self->id);
   g_object_unref(self->gconf_client);
   asbestos_tunnel_close_sockets(self);
}

static void
asbestos_tunnel_instance_init (AsbestosTunnel *self)
{
   self->sockets = NULL;
   self->entries = NULL;
   self->routes = NULL;
   self->destination = g_strdup(""); 
   self->ssh_options = g_strdup("-N -x -C -k -a");
   self->name = g_strdup("New Tunnel");
}

/* FIXME: It's really stupid to do this here, and have each tunnel fire up
 *        it's own dbus connection and then throw it away. It's also the
 *        quickest hack to get this stuff working, so I'm doing it for the
 *        moment. I'm also playing fast and loose with the error checking
 *        because I intend to move/rewrite this code.
 */
static void
send_dbus_request (gboolean add, AsbestosRoute *route)
{
   DBusGConnection *connection;
   GError *error = NULL;
   DBusGProxy *proxy;

   connection = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);
   if(connection == NULL)
   {
      g_printerr("Failed to open connection to bus: %s\n", error->message);
      g_error_free(error);
      return;
   }

   proxy = dbus_g_proxy_new_for_name(connection,
                                     "org.gnome.AsbestosRouter",
                                     "/org/gnome/AsbestosRouter",
                                     "org.gnome.AsbestosRouter");

   error = NULL;
   if(add)
   {
      if(!org_gnome_AsbestosRouter_add(proxy, 
                           route->destination,
                           (guint)(route->remote_port),
                           (guint)(route->local_port),
                           &error))
      {
         g_error("Error: %s\n", error->message);
      }
   } else {
      if(!org_gnome_AsbestosRouter_remove(proxy, 
                           route->destination,
                           (guint)(route->remote_port),
                           (guint)(route->local_port),
                           &error))
      {
         g_error("Error: %s\n", error->message);
      }
   }
   
   /* FIXME: Do I need to free connection? 
    * Does dbus magically do that for me? */
}
                   

static void
gconf_changed (GConfClient *client, guint cnxn_id, 
               GConfEntry *entry, AsbestosTunnel *self)
{
   gchar *str;
   GSList *entries, *node;

#define SNARF(xxx) \
   { \
      str = asbestos_gconf_tunnel_get_ ## xxx (client, self->id); \
      if(str == NULL) str = g_strdup(""); \
      if(self->xxx != NULL) g_free(self->xxx); \
      self->xxx = g_strdup(str); \
      g_free(str); \
   }

   SNARF(name);
   SNARF(destination);
   SNARF(ssh_options);

#undef SNARF

   entries = asbestos_gconf_tunnel_get_entries(client, self->id);

   /* FIXME: oh god, I'm going to dump my whole list & recreate it, just 
    *        because I'm too lazy to do this the right way at the moment.
    */
   asbestos_tunnel_dump_entries(self);

   for(node = entries; node != NULL; node = node->next)
   {
      guint16 port = 0;
      gchar *ptr = (gchar *)(node->data);
      while(*ptr != ':' && *ptr != '\0') ptr++;
      if(*ptr == ':')
      {
         *ptr = '\0';
         ptr++;
         port = (guint16)g_ascii_strtod(ptr, NULL);
      }
      asbestos_tunnel_add_entry(self, (gchar *)node->data, port);
   }

   g_slist_foreach(entries, (GFunc)g_free, NULL);
   g_slist_free(entries);
}

/**
 * asbestos_tunnel_new:
 *
 * @gconf_client: a gconf client
 * @id: a tunnel id in gconf
 * 
 * Returns: a newly allocated #AsbestosTunnel
 */
AsbestosTunnel *
asbestos_tunnel_new (GConfClient *gconf_client, const gchar *id)
{
   AsbestosTunnel *self = g_object_new(ASBESTOS_TUNNEL_TYPE, NULL);
   gchar *path;
   GError *error = NULL;

   g_object_ref(gconf_client);
   self->gconf_client = gconf_client;
   self->id = g_strdup(id);


   path = asbestos_gconf_tunnel_get_gconf_path(self->gconf_client, self->id);
   self->gconf_notify = gconf_client_notify_add(self->gconf_client, path,
                                           (GConfClientNotifyFunc)gconf_changed,
                                                self, NULL, &error);
   /* FIXME: handle potential errors */
   g_free(path);
   gconf_changed(self->gconf_client, self->gconf_notify, NULL, self);


   return self;
}

/**
 * asbestos_tunnel_socket_new:
 * @self: an #AsbestosTunnel
 *
 * Opens a socket & stores it in @self's list of open sockets.
 *
 * Returns: the descriptor referencing the newly opened socket
 */
gint 
asbestos_tunnel_socket_new (AsbestosTunnel *self)
{
   int *sockfd;
   
   sockfd = g_new0(int, 1);
   
   *sockfd = socket(AF_INET, SOCK_STREAM, 0);

   self->sockets = g_slist_prepend(self->sockets, sockfd);
   return *sockfd;
}

/**
 * asbestos_allocate_port:
 * @self: an #AsbestosTunnel
 *
 * Opens a socket so in order to grab ownership of an ephemeral port.
 *
 * Returns: the newly allocated port
 */
guint16 
asbestos_allocate_port (AsbestosTunnel *self)
{
   int sockfd;
   int yes = 1;
   socklen_t len;
   struct sockaddr_in my_addr;
   struct sockaddr *addr = (struct sockaddr *)&my_addr;

   sockfd = asbestos_tunnel_socket_new(self);

   my_addr.sin_family = AF_INET;
   my_addr.sin_port = htons(0);
   my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   memset(&(my_addr.sin_zero), '\0', 8);

   len = sizeof(struct sockaddr);

   if(setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1)
   {
      perror("setsockopt");
      exit(1);
   }

   if(bind(sockfd, addr, len) == -1)
   {
      perror("bind");
      exit(1);
   }

   if(getsockname(sockfd, addr, &len) == -1)
   {
      perror("getsockname");
      exit(1);
   }

   return ntohs(my_addr.sin_port);
}

gchar *
asbestos_tunnel_get_ssh_command (AsbestosTunnel *self)
{
   GSList *entries;
   AsbestosTunnelEntry *entry;
   gchar *str;
   GString *ssh = g_string_new("");

   g_string_printf(ssh, "ssh %s ", self->ssh_options);

   for(entries = self->entries; entries != NULL; entries = entries->next)
   {
      entry = (AsbestosTunnelEntry *)entries->data;

      if(entry->local_port == 0) 
      {
         entry->local_port = asbestos_allocate_port(self);
      }
      g_string_append_printf(ssh, "-L %d:%s:%d ", 
                             entry->local_port, entry->destination, 
                             entry->remote_port);
      
   }

   g_string_append(ssh, self->destination);

   str = ssh->str;
   g_string_free(ssh, FALSE);

   return str;
}

/**
 * asbestos_tunnel_close_sockets:
 * @self: an #AsbestosTunnel
 *
 * Closes all sockets that @self has opened. 
 * Note: this invalidates the local_port value in any #AsbestosTunnelEntry 
 * inside @self, since we no longer own the port number after closing its 
 * socket.
 */
void 
asbestos_tunnel_close_sockets (AsbestosTunnel *self)
{
   GSList *node;
   gint *sockfd;

   for(node = self->sockets; node != NULL; node = node->next)
   {
      sockfd = (gint *)self->sockets->data;
      close(*sockfd);
      g_free(sockfd);
   }
   g_slist_free(self->sockets);
   self->sockets = NULL;
}

/**
 * asbesos_tunnel_set_connected:
 * @self: an #AsbestosTunnel
 * @connected: a #gboolean
 *
 * Either connects or disconnects @self based on the value of @connected.
 */
void 
asbestos_tunnel_set_connected (AsbestosTunnel *self, gboolean connected)
{
   if(self->connected == connected) return;

   self->connected = connected;

   if(self->connected) /* Deal with actually connecting */
   {
      gchar *str, **argv;
      GError *error = NULL;
      GSList *node;

      str = asbestos_tunnel_get_ssh_command(self);
      for(node = self->entries; node != NULL; node = node->next)
      {
         AsbestosTunnelEntry *entry = (AsbestosTunnelEntry *) (node->data);
         AsbestosRoute *route; 

         route = asbestos_route_new();
         route->destination = g_strdup(entry->destination);
         route->remote_port = entry->remote_port;
         route->local_port = entry->local_port;

         self->routes = g_slist_prepend(self->routes, route);

         send_dbus_request(TRUE, route);
#ifdef DEBUG
         printf("Requesting %s:%d -> localhost:%d\n", 
                route->destination, route->remote_port, route->local_port);
#endif
      }

#ifdef DEBUG
      printf("Spawning ssh: '%s'\n", str);
#endif
      argv = g_strsplit(str, " ", 0);

      /* FIXME: catch the return value & handle it, catch errors & handle
       *        those, etc.
       */
      g_spawn_async_with_pipes(NULL, /* Working dir */
                               argv, /* argv */
                               NULL, /* envp */
                               G_SPAWN_SEARCH_PATH, /* spawn opts */
                               NULL, /* Run before exec */
                               NULL, /* userdata */
                               &(self->ssh_pid), /* pid */
                               NULL, /* stdin */
                               NULL, /* stdout */
                               NULL, /* stderr */
                               &error);

      /* TODO: hang off SIGCHLD so I can notice if my ssh goes away for some
       *       reason OTHER than me killing it
       */

      g_free(str);
      g_strfreev(argv);
   } else { /* Deal with disconnecting */
      GSList *node;

      if(self->ssh_pid != 0)
      {
         kill(self->ssh_pid, SIGHUP);
         g_spawn_close_pid(self->ssh_pid);
         self->ssh_pid = 0;
      }

      for(node = self->routes; node != NULL; node = node->next)
      {
         AsbestosRoute *route = (AsbestosRoute *)(node->data);

         send_dbus_request(FALSE, route);

#ifdef DEBUG
         printf("Hanging up %s:%d -> localhost:%d\n", 
                route->destination, route->remote_port, route->local_port);
#endif
         asbestos_route_free(route);
      }
      g_slist_free(self->routes);
      self->routes = NULL;
   }
}

/**
 * asbestos_tunnel_connect:
 * @self: an #AsbestosTunnel
 *
 * Convenience wrapper around asbestos_tunnel_set_connected; simply calls
 * that with %TRUE
 */
void 
asbestos_tunnel_connect (AsbestosTunnel *self)
{
   asbestos_tunnel_set_connected(self, TRUE);
}

/**
 * asbestos_tunnel_disconnect:
 * @self: an #AsbestosTunnel
 *
 * Convenience wrapper around asbestos_tunnel_set_connected; simply calls
 * that with %FALSE
 */
void 
asbestos_tunnel_disconnect (AsbestosTunnel *self)
{
   asbestos_tunnel_set_connected(self, FALSE);
}

/**
 * asbestos_tunnel_add_entry:
 * @self: an #AsbestosTunnel
 * @hostname: a string representing a remote host
 * @remote_port: a port number on the remote host
 *
 * Adds an entry to @self for connecting to @hostname on port @remote_port.
 *
 * Returns: %TRUE if the entry was succesfully added, %FALSE if not (typically
 *          indicating that @self already contains the specified entry)
 */
gboolean 
asbestos_tunnel_add_entry (AsbestosTunnel *self, const gchar *hostname, 
                           guint16 remote_port)
{
   AsbestosTunnelEntry *entry;
   GSList *entries = NULL;

   for(entries = self->entries; entries != NULL; entries = entries->next)
   {
      entry = ASBESTOS_TUNNEL_ENTRY(entries->data);
      if(entry->remote_port != remote_port) continue;
      if(!g_str_equal(entry->destination, hostname)) /* dupe hostname */
      {
         /* TODO: is it worth doing anything extra to figure out duplicate hosts
	  *       in a smarter way? (i.e. lookup ips, compare THOSE for 
	  *       duplicates) I suppose it could get fancy when hostname A 
          *       resolves to ip 1,2,3 & hostname B resolves to ip 3,4,5; there
	  *       IS some overlap, but it A != B
	  */
         continue;
      }
      
      /* We've already got this entry */
      return FALSE;
   }

   entry = g_new0(AsbestosTunnelEntry, 1);
   entry->destination = g_strdup(hostname);
   entry->remote_port = remote_port;
#ifdef DEBUG
   printf("adding entry: %s:%d\n", entry->destination, entry->remote_port);
#endif

   self->entries = g_slist_prepend(self->entries, entry);
   return TRUE;
}

/**
 * asbestos_tunnel_dump_entries:
 * @self: an #AsbestosTunnel
 *
 * Removes all entries in @self
 */
void 
asbestos_tunnel_dump_entries (AsbestosTunnel *self)
{
   g_slist_foreach(self->entries, (GFunc)(asbestos_tunnel_entry_free), NULL);
   g_slist_free(self->entries);
   self->entries = NULL;
}

/**
 * asbestos_tunnel_entry_free:
 * @self: an #AsbestosTunnelEntry
 *
 * Frees the memory used by @self
 */
void
asbestos_tunnel_entry_free (AsbestosTunnelEntry *self)
{
   if(self == NULL) return;
   g_free(self->destination);
   g_free(self);
}
