#ifndef ASBESTOS_WINDOW_H_
#define ASBESTOS_WINDOW_H_

#include <glib-object.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include "asbestos_tunnel.h"

#define ASBESTOS_WINDOW_TYPE		(asbestos_window_get_type())
#define ASBESTOS_WINDOW(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ASBESTOS_WINDOW_TYPE, AsbestosWindow))
#define ASBESTOS_WINDOW_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ASBESTOS_WINDOW_TYPE, AsbestosWindowClass))
#define IS_ASBESTOS_WINDOW(obj)		(G_TYPE_CHECK_TYPE ((obj), ASBESTOS_WINDOW_TYPE))
#define IS_ASBESTOS_WINDOW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ASBESTOS_WINDOW_TYPE))
#define ASBESTOS_WINDOW_GET_CLASS(obj)	(G_TYPE_CHECK_GET_CLASS ((obj), ASBESTOS_WINDOW_TYPE, AsbestosWindowClass))

typedef struct _AsbestosWindow AsbestosWindow;
typedef struct _AsbestosWindowClass AsbestosWindowClass;

struct _AsbestosWindow
{
   GtkWindow parent;

   GtkTreeView *tree_view;
   GtkTreeSelection *selection;
   GtkListStore *list_store;

   GConfClient *gconf_client;
   gint gconf_notify;

   GtkWidget *new_button;
   GtkWidget *properties_button;
   GtkWidget *delete_button;
};

struct _AsbestosWindowClass
{
   GtkWindowClass parent;
};


GType asbestos_window_get_type (void);
AsbestosWindow *asbestos_window_new (void);

#endif
