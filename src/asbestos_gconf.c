#include "config.h"
#include "asbestos_gconf.h"

gchar  *
asbestos_gconf_tunnel_get_gconf_path (GConfClient *client, const gchar *id)
{
   gchar *path;

   path = g_strdup_printf("%s/tunnels/%s", ASBESTOS_GCONF_PATH, id);
   return path;
}


void
asbestos_gconf_tunnel_set_string (GConfClient *client,
                                  const gchar *id,
                                  const gchar *key,
                                  const gchar *value)
{
   gchar *real_key;
   GError *error = NULL;

   g_assert(client != NULL);
   g_assert(id != NULL);
   g_assert(key != NULL);
   g_assert(value != NULL);

   real_key = g_strdup_printf("%s/tunnels/%s/%s", ASBESTOS_GCONF_PATH, 
                                                  id, key);

   if(!gconf_client_set_string(client, real_key, value, &error))
   {
      /* FIXME: cope with whatever this error was */
   }

   g_free(real_key);
}

gchar *
asbestos_gconf_tunnel_get_string (GConfClient *client,
                                  const gchar *id,
                                  const gchar *key)
{
   gchar *real_key, *value;
   GError *error = NULL;

   g_assert(client != NULL);
   g_assert(id != NULL);
   g_assert(key != NULL);

   real_key = g_strdup_printf("%s/tunnels/%s/%s", ASBESTOS_GCONF_PATH, 
                                                  id, key);

   value = gconf_client_get_string(client, real_key, &error);
   /* FIXME: catch errors */

   g_free(real_key);
   return value;
}


gchar *
asbestos_gconf_create_tunnel (GConfClient *client)
{
   gchar *id = NULL;
   GError *error = NULL;
   int i;


   for(i = 0; !id; i++)
   {
      gchar *dir;
      id = g_strdup_printf("tunnel_%d", i);
      dir = g_strdup_printf("%s/tunnels/%s", ASBESTOS_GCONF_PATH, id);
      if(gconf_client_dir_exists(client, dir, &error)) // FIXME: handle errors
      {
         g_free(id);
         id = NULL;
      }

      g_free(dir);
   }

   g_assert(id != NULL);

   asbestos_gconf_tunnel_set_name(client, id, "New Tunnel");
   asbestos_gconf_tunnel_set_destination(client, id, "Some Host");

   asbestos_gconf_add_tunnel_to_list(client, id);

   return id;
}


void 
asbestos_gconf_add_tunnel_to_list (GConfClient *client, const gchar *id)
{
   GSList *ids;
   GError *error = NULL;
   gchar *new_id;
   gchar *key;

   new_id = g_strdup(id); /* Since it's const... */
   ids = asbestos_gconf_get_tunnels(client);
   /* FIXME: handle errors */

   
   ids = g_slist_append(ids, new_id);
   key = g_strdup_printf("%s/tunnels/list", ASBESTOS_GCONF_PATH);
   if(!gconf_client_set_list(client, key, GCONF_VALUE_STRING, ids, &error))
   {
      /* FIXME: handle errors */
   }

   g_slist_foreach(ids, (GFunc)g_free, NULL);
   g_slist_free(ids);
   g_free(key);
}

void 
asbestos_gconf_remove_tunnel_from_list (GConfClient *client, const gchar *id)
{
   GSList *ids, *node;
   GError *error = NULL;
   gchar *key;
   gboolean found = FALSE;

   ids = asbestos_gconf_get_tunnels(client);
   /* FIXME: handle errors */

   
   ids = g_slist_remove(ids, id);

   for(node = ids; node != NULL; node = node->next)
   {
      if(!g_str_equal(id, (gchar *)(node->data))) continue;

      found = TRUE;
      g_free(node->data);
      ids = g_slist_delete_link(ids, node);
      break;
   }

   if(found)
   {

      key = g_strdup_printf("%s/tunnels/list", ASBESTOS_GCONF_PATH);
      if(!gconf_client_set_list(client, key, GCONF_VALUE_STRING, ids, &error))
      {
         /* FIXME: handle errors */
      }
      g_free(key);

      key = g_strdup_printf("%s/tunnels/%s/destination", ASBESTOS_GCONF_PATH, 
                            id);
      if(!gconf_client_unset(client, key, &error))
      {
         /* FIXME: handle errors */
      }
      g_free(key);
      key = g_strdup_printf("%s/tunnels/%s/entries", ASBESTOS_GCONF_PATH, 
                            id);
      if(!gconf_client_unset(client, key, &error))
      {
         /* FIXME: handle errors */
      }
      g_free(key);
      key = g_strdup_printf("%s/tunnels/%s/name", ASBESTOS_GCONF_PATH, 
                            id);
      if(!gconf_client_unset(client, key, &error))
      {
         /* FIXME: handle errors */
      }
      g_free(key);
      key = g_strdup_printf("%s/tunnels/%s/ssh_options", ASBESTOS_GCONF_PATH, 
                            id);
      if(!gconf_client_unset(client, key, &error))
      {
         /* FIXME: handle errors */
      }
      g_free(key);
   }

   g_slist_foreach(ids, (GFunc)g_free, NULL);
   g_slist_free(ids);
}

GSList *
asbestos_gconf_get_tunnels (GConfClient *client)
{
   GSList *ids;
   gchar *key;
   GError *error = NULL;

   key = g_strdup_printf("%s/tunnels/list", ASBESTOS_GCONF_PATH);
   ids = gconf_client_get_list(client, key, GCONF_VALUE_STRING, &error);
   /* FIXME: handle possible errors */

   g_free(key);

   return ids;
}

GSList *
asbestos_gconf_tunnel_get_entries (GConfClient *client, const gchar *id)
{
   GSList *entries;
   gchar *key;
   GError *error = NULL;

   key = g_strdup_printf("%s/tunnels/%s/entries", ASBESTOS_GCONF_PATH, id);
   entries = gconf_client_get_list(client, key, GCONF_VALUE_STRING, &error);
   /* FIXME: handle possible errors */

   g_free(key);

   return entries;
}

void 
asbestos_gconf_tunnel_set_entries (GConfClient *client, const gchar *id,
                                   GSList *entries)
{
   gchar *key;
   GError *error = NULL;

   key = g_strdup_printf("%s/tunnels/%s/entries", ASBESTOS_GCONF_PATH, id);
   if(!gconf_client_set_list(client, key, GCONF_VALUE_STRING, entries, &error))
   {
      /* FIXME: handle error */
   }
   g_free(key);
}
