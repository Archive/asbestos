#ifndef __ASBESTOS_GCONF_H
#define __ASBESTOS_GCONF_H

#include <gconf/gconf-client.h>

#define ASBESTOS_GCONF_PATH "/apps/asbestos"

gchar *asbestos_gconf_tunnel_get_gconf_path (GConfClient *client, 
                                             const gchar *id);
void asbestos_gconf_tunnel_set_string (GConfClient *client,
                                       const gchar *id,
                                       const gchar *key,
                                       const gchar *value);
gchar *asbestos_gconf_tunnel_get_string (GConfClient *client,
                                         const gchar *id,
                                         const gchar *key);

/* FIXME: I might want to make these real functions so I can sanity check
 *        the strings dependent on their final destination
 */
#define asbestos_gconf_tunnel_set_name(client, id, name) \
   asbestos_gconf_tunnel_set_string((client), (id), "name", (name))
#define asbestos_gconf_tunnel_get_name(client, id) \
   asbestos_gconf_tunnel_get_string((client), (id), "name")
#define asbestos_gconf_tunnel_set_destination(client, id, dest) \
   asbestos_gconf_tunnel_set_string((client), (id), "destination", (dest))
#define asbestos_gconf_tunnel_get_destination(client, id) \
   asbestos_gconf_tunnel_get_string((client), (id), "destination")
#define asbestos_gconf_tunnel_set_ssh_options(client, id, opts) \
   asbestos_gconf_tunnel_set_string((client), (id), "ssh_options", (opts))
#define asbestos_gconf_tunnel_get_ssh_options(client, id) \
   asbestos_gconf_tunnel_get_string((client), (id), "ssh_options")

gchar *asbestos_gconf_create_tunnel (GConfClient *client);
void asbestos_gconf_add_tunnel_to_list (GConfClient *client, const gchar *id);
void asbestos_gconf_remove_tunnel_from_list (GConfClient *client, 
                                             const gchar *id);
GSList *asbestos_gconf_get_tunnels (GConfClient *client);
GSList *asbestos_gconf_tunnel_get_entries (GConfClient *client, 
                                           const gchar *id);
void asbestos_gconf_tunnel_set_entries (GConfClient *client, const gchar *id,
                                        GSList *entries);

#endif
