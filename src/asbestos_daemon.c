#define DBUS_API_SUBJECT_TO_CHANGE
#include "config.h"
#include <stdio.h>
#include <gnome.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "asbestos_router.h"

int
main (int argc, char **argv)
{
   AsbestosRouter *router;
   DBusGConnection *connection;
   GError *error;
   DBusGProxy *proxy;
   guint32 ret;

   g_type_init();

   router = asbestos_router_new();

   error = NULL;
   connection = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);

   if(connection == NULL)
   {
      g_printerr("Failed to open connection to bus: %s\n", error->message);
      g_error_free(error);
      exit(1);
   }

   dbus_g_connection_register_g_object(connection,
                                       "/org/gnome/AsbestosRouter",
                                       G_OBJECT(router));

   proxy = dbus_g_proxy_new_for_name(connection,
                                     DBUS_SERVICE_DBUS,
                                     DBUS_PATH_DBUS,
                                     DBUS_INTERFACE_DBUS);

   /* TODO: error checking */
   if(!org_freedesktop_DBus_request_name(proxy, "org.gnome.AsbestosRouter",
                                         0, &ret, &error))
   {
      g_error("Error getting name: %s\n", error->message);
   }

   g_main_loop_run(g_main_loop_new(NULL, FALSE));
   g_object_unref(router);
   
   return 0;
}
