#ifndef ASBESTOS_ROUTER_H_
#define ASBESTOS_ROUTER_H_

#include <glib-object.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>

#define ASBESTOS_ROUTER_TYPE		(asbestos_router_get_type())
#define ASBESTOS_ROUTER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ASBESTOS_ROUTER_TYPE, AsbestosRouter))
#define ASBESTOS_ROUTER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ASBESTOS_ROUTER_TYPE, AsbestosRouterClass))
#define IS_ASBESTOS_ROUTER(obj)		(G_TYPE_CHECK_TYPE ((obj), ASBESTOS_ROUTER_TYPE))
#define IS_ASBESTOS_ROUTER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ASBESTOS_ROUTER_TYPE))
#define ASBESTOS_ROUTER_GET_CLASS(obj)	(G_TYPE_CHECK_GET_CLASS ((obj), ASBESTOS_ROUTER_TYPE, AsbestosRouterClass))

typedef struct _AsbestosRouter AsbestosRouter;
typedef struct _AsbestosRouterClass AsbestosRouterClass;
typedef struct _AsbestosRoute AsbestosRoute;

struct _AsbestosRouter
{
   GObject parent;

   GSList *routes; /* FIXME: I probably should store this in something a little
                    *        better at random access
                    */

};

struct _AsbestosRouterClass
{
   GObjectClass parent;
};

struct _AsbestosRoute
{
   gchar *destination;
   guint16 remote_port;
   guint16 local_port;
};

GType asbestos_router_get_type (void);
AsbestosRouter *asbestos_router_new (void);
GSList *asbestos_router_get_routes (AsbestosRouter *self);
GSList *asbestos_router_has_route (AsbestosRouter *self, const gchar *ip,
                                   guint16 remote_port, guint16 local_port);
gboolean asbestos_router_add_route (AsbestosRouter *self, const gchar *ip,
                                    guint16 remote_port, guint16 local_port);
gboolean asbestos_router_remove_route (AsbestosRouter *self, const gchar *ip,
                                       guint16 remote_port, guint16 local_port);

AsbestosRoute *asbestos_route_new (void);
void asbestos_route_free (AsbestosRoute *route);

#endif
