#ifndef ASBESTOS_TUNNEL_H_
#define ASBESTOS_TUNNEL_H_

#include <glib-object.h>
#include <gtk/gtk.h>
#include "asbestos_gconf.h"

#define ASBESTOS_TUNNEL_TYPE		(asbestos_tunnel_get_type())
#define ASBESTOS_TUNNEL(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ASBESTOS_TUNNEL_TYPE, AsbestosTunnel))
#define ASBESTOS_TUNNEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ASBESTOS_TUNNEL_TYPE, AsbestosTunnelClass))
#define IS_ASBESTOS_TUNNEL(obj)		(G_TYPE_CHECK_TYPE ((obj), ASBESTOS_TUNNEL_TYPE))
#define IS_ASBESTOS_TUNNEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ASBESTOS_TUNNEL_TYPE))
#define ASBESTOS_TUNNEL_GET_CLASS(obj)	(G_TYPE_CHECK_GET_CLASS ((obj), ASBESTOS_TUNNEL_TYPE, AsbestosTunnelClass))

typedef struct _AsbestosTunnel AsbestosTunnel;
typedef struct _AsbestosTunnelClass AsbestosTunnelClass;
typedef struct _AsbestosTunnelEntry AsbestosTunnelEntry;

struct _AsbestosTunnel
{
   GObject parent;

   GConfClient *gconf_client;
   gchar *id;

   GSList *sockets;
   GSList *entries;
   GSList *routes;
   gchar *name;

   guint gconf_notify;

   gboolean connected;
   gchar *destination;
   gchar *ssh_options;

   GPid ssh_pid;
};

struct _AsbestosTunnelClass
{
   GObjectClass parent;
};

#define ASBESTOS_TUNNEL_ENTRY(obj) ((AsbestosTunnelEntry *)(obj))
struct _AsbestosTunnelEntry
{
   gchar *destination;
   guint16 remote_port;
   guint16 local_port;
};


GType asbestos_tunnel_get_type (void);
AsbestosTunnel *asbestos_tunnel_new (GConfClient *gconf_client, 
                                     const gchar *id);

guint16 asbestos_allocate_port (AsbestosTunnel *self);
gint asbestos_tunnel_socket_new (AsbestosTunnel *self);
gboolean asbestos_tunnel_add_entry (AsbestosTunnel *self, const gchar *hostname,
                                    guint16 port);
gchar *asbestos_tunnel_get_ssh_command (AsbestosTunnel *self);
void asbestos_tunnel_close_sockets (AsbestosTunnel *self);
void asbestos_tunnel_set_connected (AsbestosTunnel *self, gboolean connected);
void asbestos_tunnel_connect (AsbestosTunnel *self);
void asbestos_tunnel_disconnect (AsbestosTunnel *self);
void asbestos_tunnel_dump_entries (AsbestosTunnel *self);
void asbestos_tunnel_entry_free (AsbestosTunnelEntry *self);

/* TODO: Move these elsewhere */
GSList *asbestos_host_to_ips (const gchar *hostname);

#endif
