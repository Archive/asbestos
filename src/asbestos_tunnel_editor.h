#ifndef ASBESTOS_TUNNEL_EDITOR_H_
#define ASBESTOS_TUNNEL_EDITOR_H_

#include <glib-object.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>

#define ASBESTOS_TUNNEL_EDITOR_TYPE		(asbestos_tunnel_editor_get_type())
#define ASBESTOS_TUNNEL_EDITOR(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), ASBESTOS_TUNNEL_EDITOR_TYPE, AsbestosTunnelEditor))
#define ASBESTOS_TUNNEL_EDITOR_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ASBESTOS_TUNNEL_EDITOR_TYPE, AsbestosTunnelEditorClass))
#define IS_ASBESTOS_TUNNEL_EDITOR(obj)		(G_TYPE_CHECK_TYPE ((obj), ASBESTOS_TUNNEL_EDITOR_TYPE))
#define IS_ASBESTOS_TUNNEL_EDITOR_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ASBESTOS_TUNNEL_EDITOR_TYPE))
#define ASBESTOS_TUNNEL_EDITOR_GET_CLASS(obj)	(G_TYPE_CHECK_GET_CLASS ((obj), ASBESTOS_TUNNEL_EDITOR_TYPE, AsbestosTunnelEditorClass))

typedef struct _AsbestosTunnelEditor AsbestosTunnelEditor;
typedef struct _AsbestosTunnelEditorClass AsbestosTunnelEditorClass;

struct _AsbestosTunnelEditor
{
   GtkWindow parent;

   GConfClient *gconf_client;
   guint gconf_notify;
   gchar *id;
   GtkTreeView *tree_view;
   GtkListStore *list_store;
   GtkTreeSelection *selection;

   GtkWidget *add_button;
   GtkWidget *remove_button;

   GtkWidget *name_entry;
   GtkWidget *destination_entry;
   GtkWidget *ssh_options_entry;

   GtkWidget *save_button;
   GtkWidget *cancel_button;

};

struct _AsbestosTunnelEditorClass
{
   GtkWindowClass parent;
};


GType asbestos_tunnel_editor_get_type (void);
AsbestosTunnelEditor *asbestos_tunnel_editor_new (GConfClient *gconf_client,
                                                  const gchar *id);
void asbestos_tunnel_editor_add_row (AsbestosTunnelEditor *self, 
                                     const gchar *dest, guint16 port);


#endif
