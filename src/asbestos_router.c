#include "config.h"
#include "asbestos_router.h"
#include "asbestos_gconf.h"
#include <libgnome/gnome-macros.h>
#include <glade/glade-xml.h>
#include <glib/gi18n.h>
#include <gtk/gtkmarshal.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>

gboolean asbestos_router_add (AsbestosRouter *router, gchar *destination,
                     guint remote_port, guint local_port, GError **error);
gboolean asbestos_router_remove (AsbestosRouter *router, gchar *destination,
                     guint remote_port, guint local_port, GError **error);

#include "asbestos_router_dbus_glue.h"

static void asbestos_router_connect_route (AsbestosRouter *self, 
                                           AsbestosRoute *route);
static void asbestos_router_disconnect_route (AsbestosRouter *self, 
                                              AsbestosRoute *route);

GNOME_CLASS_BOILERPLATE(AsbestosRouter,
                        asbestos_router,
			GObject,
			G_TYPE_OBJECT);

static void asbestos_router_class_init (AsbestosRouterClass *klass);
static void asbestos_router_finalize (GObject *obj);

static void 
asbestos_router_class_init (AsbestosRouterClass *klass)
{
   GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = asbestos_router_finalize;

   dbus_g_object_class_install_info(gobject_class,
                                    &dbus_glib_asbestos_router_object_info);
}

static void 
asbestos_router_finalize (GObject *obj)
{
   AsbestosRouter *self = ASBESTOS_ROUTER(obj);

   while(self->routes != NULL)
   {
      AsbestosRoute *route = (AsbestosRoute *)(self->routes->data);
      asbestos_router_remove_route(self, route->destination, 
                                   route->remote_port, route->local_port);
   }

   if(G_OBJECT_CLASS (parent_class)->finalize)
   {
      (* G_OBJECT_CLASS (parent_class)->finalize)(obj);
   }
}

static void
asbestos_router_instance_init (AsbestosRouter *self)
{
   self->routes = NULL;
}

gboolean
asbestos_router_add (AsbestosRouter *router, gchar *destination,
                     guint remote_port, guint local_port, GError **error)
{
   asbestos_router_add_route(router, destination, 
                             (guint16)remote_port, (guint16)local_port);
   return TRUE;
}

gboolean
asbestos_router_remove (AsbestosRouter *router, gchar *destination,
                        guint remote_port, guint local_port, GError **error)
{
   asbestos_router_remove_route(router, destination, 
                               (guint16)remote_port, (guint16)local_port);
   return TRUE;
}


/**
 * asbestos_router_new:
 * 
 * Returns: a newly allocated #AsbestosRouter
 */
AsbestosRouter *
asbestos_router_new (void)
{
   AsbestosRouter *self;

   self = g_object_new(ASBESTOS_ROUTER_TYPE, NULL);


   return self;
}

GSList *
asbestos_router_get_routes (AsbestosRouter *self)
{
   /* Why do I even have this function? */
   return self->routes;
}

GSList * 
asbestos_router_has_route (AsbestosRouter *self, const gchar *destination,
                           guint16 remote_port, guint16 local_port)
{
   GSList *node;
   gboolean has_route = FALSE;

   /* Yeah, I _REALLY_ should store my routes in a saner way */
   for(node = self->routes; node != NULL; node = node->next)
   {
      AsbestosRoute *route = (AsbestosRoute *)(node->data);
      if(remote_port != route->remote_port) continue;
      if(local_port != route->local_port) continue;
      if(!g_str_equal(route->destination, destination)) continue;
      has_route = TRUE;
      break;
   }
   
   if(has_route) return node;
   return NULL;
}

/**
 * asbestos_router_add_route:
 * @self: an #AsbestosRouter
 * @destination: the remote host name
 * @remote_port: the port on the remote host we want to intercept
 * @local_port: the port on localhost to redirect to
 * 
 * Returns: %TRUE on success, %FALSE on failure (already have that route,
 *          @destination is invalid, etc.) FIXME: I probably want to send back 
 *          more detail than just a boolean
 */
gboolean 
asbestos_router_add_route (AsbestosRouter *self, const gchar *destination,
                           guint16 remote_port, guint16 local_port)
{
   AsbestosRoute *route;
   if(asbestos_router_has_route(self, destination, remote_port, local_port) 
      != NULL)
   {
      return FALSE;
   }

   route = asbestos_route_new();
   route->destination = g_strdup(destination);
   route->remote_port = remote_port;
   route->local_port = local_port;
   
   self->routes = g_slist_prepend(self->routes, route);

   asbestos_router_connect_route(self, route);
   
   return TRUE;
}

gboolean 
asbestos_router_remove_route (AsbestosRouter *self, const gchar *destination,
                              guint16 remote_port, guint16 local_port)
{
   AsbestosRoute *route;
   GSList *node;

   node = asbestos_router_has_route(self, destination, remote_port, local_port);

   if(node == NULL) return FALSE;

   route = (AsbestosRoute *)(node->data);

   asbestos_router_disconnect_route(self, route);

   self->routes = g_slist_delete_link(self->routes, node);
   return TRUE;
}

AsbestosRoute *
asbestos_route_new (void)
{
   AsbestosRoute *route = g_new0(AsbestosRoute, 1);

   return route;
}

void 
asbestos_route_free (AsbestosRoute *route)
{
   if(route->destination != NULL) g_free(route->destination);
   g_free(route);
}

static void 
asbestos_router_connect_route (AsbestosRouter *self, AsbestosRoute *route)
{
   gchar *command;
   GError *error = NULL;

   /* Ideally, I'll link in itpables support so I can just do this, but for
    * now, I can build up a command line.
    */
   command = g_strdup_printf("/sbin/iptables -t nat -I OUTPUT -j DNAT -p tcp "
                             "--destination %s "
                             "--destination-port %d "
                             "--to-destination 127.0.0.1:%d",
                             route->destination, 
                             route->remote_port, 
                             route->local_port);
#ifdef DEBUG
   printf("Connecting: %s:%d -> localhost:%d\n",
          route->destination, route->remote_port, route->local_port);
   printf("%s\n", command);
#endif

   /* TODO: actually trap and deal with errors */
   g_spawn_command_line_async(command, &error);

   g_free(command);
}

static void 
asbestos_router_disconnect_route (AsbestosRouter *self, AsbestosRoute *route)
{
   gchar *command;
   GError *error = NULL;

#ifdef DEBUG
   printf("Disconnecting: %s:%d -> localhost:%d\n", 
          route->destination, route->remote_port, route->local_port);
#endif

   command = g_strdup_printf("/sbin/iptables -t nat -D OUTPUT -j DNAT -p tcp "
                             "--destination %s "
                             "--destination-port %d "
                             "--to-dest 127.0.0.1:%d",
                             route->destination, 
                             route->remote_port, 
                             route->local_port);

   /* TODO: actually trap and deal with errors */
   g_spawn_command_line_async(command, &error);

   g_free(command);
}
